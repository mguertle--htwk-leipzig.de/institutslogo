sizes=100 200 500
dpi=300 600
types=png pdf gif
all: embLogos.zip tpmbLogos.zip


tpmb: input/tpmb.svg
	$(foreach i,$(sizes), \
	$(foreach j, $(dpi), \
	$(foreach k, $(types), \
	convert -density $(j) $<  -resize $(i)^ output/$@_$(i)_$(j)dpi.$(k); \
	convert -density $(j) $<  -transparent white -resize $(i)^ output/$@_$(i)_$(j)dpi_t.$(k);\
	)))

tpmbNurLogo: input/tpmbNurLogo.svg
	-mkdir output
	$(foreach i,$(sizes), \
	$(foreach j, $(dpi), \
	$(foreach k, $(types), \
	convert -density $(j) $<  -resize $(i)^ output/$@_$(i)_$(j)dpi.$(k); \
	convert -density $(j) $<  -transparent white -resize $(i)^ output/$@_$(i)_$(j)dpi_t.$(k); \
	\
	)))


emb: input/emb.svg
	-mkdir output
	$(foreach i,$(sizes), \
	$(foreach j, $(dpi), \
	$(foreach k, $(types), \
	convert -density $(j) $<  -resize $(i)^ output/$@_$(i)_$(j)dpi.$(k); \
	convert -density $(j) $<  -transparent white -resize $(i)^ output/$@_$(i)_$(j)dpi_t.$(k);\
	)))

embNurLogo: input/embNurLogo.svg
	-mkdir output
	$(foreach i,$(sizes), \
	$(foreach j, $(dpi), \
	$(foreach k, $(types), \
	convert -density $(j) $<  -resize $(i)^ output/$@_$(i)_$(j)dpi.$(k); \
	convert -density $(j) $<  -transparent white -resize $(i)^ output/$@_$(i)_$(j)dpi_t.$(k); \
	\
	)))

embLogos.zip: emb embNurLogo
	zip $@ input/emb* output/emb*
	@-rm output/*


tpmbLogos.zip: tpmb tpmbNurLogo
	zip $@ input/tpmb* output/tpmb*
	@-rm output/*

clean:
	rm output/* *.zip
